{
  description = "deriveicyn";

  outputs = { self }:
  {
    strok = {
      djenyreicyn = 10;
      spici = "iuniksBildyr";
    };

    datom = {
      hyraizyn,
      niksBildyr
    }@topYrgz:
    let
      system = hyraizyn.astra.sistym;

    in
    yrgz: derivation
    {
      name = builtins.concatStringsSep "-"
      (with yrgz; [ neim vyrzyn ]);

      builder = niksBildyr;

      inherit yrgz system;

      __structuredAttrs = true;

    };
  };
}
